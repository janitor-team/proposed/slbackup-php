{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
    {include file="header.tpl"}

    <FORM action="index.php" method=post>
	<H2>{t}Maintenance{/t}</H2>
	{html_options name=client values=$clients output=$clients selected=$client}
	<INPUT type=submit name=maint value="{t}Choose{/t}">
	{if $client <> ""}
	<INPUT type=hidden name=selected value="{$client}">
        {t}Oldest snapshot to keep:{/t}</TD>
	{html_options name=snapshot values=$snapshots output=$snapshots selected=$snapshot}
	<INPUT type=submit name=removesnapshot value="{t}Delete older{/t}">
	{/if}
	<HR>
	{foreach item=removedss from=$removed}
	    {$removedss}<BR>
	{/foreach}
    </FORM>
</HTML>
